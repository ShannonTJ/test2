﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication3.Models;

namespace WebApplication3.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public string Index()
        {
            return "navigate to an url to show an example";
        }

        public ViewResult AutoProperty()
        {
            //Initialize product in one line
            Product myProduct = new Product
            {
                ProductID = 100,
                Name = "Kayak",
                Description = "A boat for one person",
                Price = 275M,
                Category = "Watersports"
            };

            return View("Result", (object)String.Format("Category: {0}", myProduct.Category));
        }

        //Extension method
        /*public ViewResult UseExtension()
        {
            //Initialize shopping cart
            ShoppingCart cart = new ShoppingCart
            {
                Products = new List<Product>
                {
                    new Product {Name = "Kayak", Price = 275M},
                    new Product {Name = "Lifejacket", Price = 48.95M},
                    new Product {Name = "Soccer ball", Price = 19.50M},
                    new Product {Name = "Flag", Price = 34.95M}
                }
            };

            //Get total price of objects in cart
            //method is called on ShoppingCart as though it is in the SC class
            decimal cartTotal = cart.TotalPrices();

            return View("Result", (object)String.Format("Total: {0:c}", cartTotal));
        }*/

        //Extension method
        public ViewResult UseExtensionEnumerable()
        {
            //Initialize shopping cart
            IEnumerable<Product> products = new ShoppingCart
            {
                Products = new List<Product>
                {
                    new Product {Name = "Kayak", Price = 275M},
                    new Product {Name = "Lifejacket", Price = 48.95M},
                    new Product {Name = "Soccer ball", Price = 19.50M},
                    new Product {Name = "Flag", Price = 34.95M}
                }
            };

            Product[] productArray =
                {
                    new Product {Name = "Kayak", Price = 275M},
                    new Product {Name = "Lifejacket", Price = 48.95M},
                    new Product {Name = "Soccer ball", Price = 19.50M},
                    new Product {Name = "Flag", Price = 34.95M}
                };

            //Get total price of objects in cart
            decimal cartTotal = products.TotalPrices();
            decimal arrayTotal = products.TotalPrices();

            return View("Result", (object)String.Format("Cart Total: {0}, Array Total: {1}", cartTotal, arrayTotal));
        }

        //Extension method
        public ViewResult UseFilterExtension()
        {
            //Initialize shopping cart
            IEnumerable<Product> products = new ShoppingCart
            {
                Products = new List<Product>
                {
                    new Product {Name = "Kayak", Price = 275M, Category = "Watersports"},
                    new Product {Name = "Lifejacket", Price = 48.95M, Category = "Watersports"},
                    new Product {Name = "Soccer ball", Price = 19.50M, Category = "Soccer"},
                    new Product {Name = "Flag", Price = 34.95M, Category = "Soccer"}
                }
            };

            decimal total = 0;

            /*
            //Get price of all Soccer category items
            foreach (Product prod in products.FilterByCategory("Soccer"))
                total += prod.Price;
            */

            /*Func<Product, bool> categoryFilter = delegate (Product prod)
            { return prod.Category == "Soccer"; };
            */

            //Lambda expression: matches objects in the Soccer category, with a price over $20
            foreach (Product prod in products.Filter(prod => prod.Category == "Soccer" || prod.Price > 20))
                total += prod.Price;

            return View("Result", (object)String.Format("Total: {0}", total));
        }

        //Array of anonymously typed objects
        public ViewResult CreateAnonArray()
        {
            var random = new[]
            {
                new{ Name = "MVC", Category = "Pattern" },
                new{ Name = "Hat", Category = "Clothing"}
            };

            StringBuilder result = new StringBuilder();

            foreach (var item in random)
                result.Append(item.Name).Append(" ");

            return View("Result", (object)result.ToString());
        }

        public ViewResult FindProducts()
        {

            Product[] products =
                {
                    new Product {Name = "Kayak", Price = 275M},
                    new Product {Name = "Lifejacket", Price = 48.95M},
                    new Product {Name = "Soccer ball", Price = 19.50M},
                    new Product {Name = "Flag", Price = 34.95M}
                };

            //Linq query (query syntax)
            /*
            var foundProducts = from match in products
                                orderby match.Price descending
                                select new { match.Name, match.Price };
            */

            //Linq query (dot notation)
            //Get the top 3 most expensive items
            //Deferred extension method
            var foundProducts = products.OrderByDescending(e => e.Price)
                                .Take(3)
                                .Select(e => new { e.Name, e.Price });

            products[2] = new Product { Name = "Stadium", Price = 80000M };

            StringBuilder result = new StringBuilder();

            //Enumerating results
            foreach (var p in foundProducts)
                result.AppendFormat("Price: {0}", p.Price);

            return View("Result", (object)result.ToString());

        }
    }
}