﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication3.Models
{
    public static class ExtensionMethods
    {
        //Gives total price of everything in shopping cart
        //"this" keyword denotes TotalPrices as extension method
        //1st parameter denotes which class the method is applied to
        public static decimal TotalPrices(this IEnumerable<Product> productEnum)
        {
            decimal total = 0;

            //for loop works directly on Product objects (includes ShoppingCart and arrays of Products)
            foreach (Product prod in productEnum)
                total += prod.Price;

            return total;
        }

        //Returns Products whose Category matches the parameter
        public static IEnumerable<Product> FilterByCategory(this IEnumerable<Product> productEnum, string categoryParam)
        {
            foreach (Product prod in productEnum)
            {
                if (prod.Category == categoryParam)
                    yield return prod;
            }
        }

        //Func = filtering parameter
        public static IEnumerable<Product> Filter(this IEnumerable<Product> productEnum, Func<Product, bool> selectorParam)
        {
            foreach (Product prod in productEnum)
            {
                //True if product should be included in the results
                if (selectorParam(prod))
                    yield return prod;
            }
        }
    }
}