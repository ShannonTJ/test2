﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Threading.Tasks;

namespace WebApplication3.Models
{
    public class AsyncMethods
    {
        //When using await, need to use the async keyword
        public async static Task<long?> GetPageLength()
        {
            HttpClient client = new HttpClient();

            //await keyword = wait for the result of GetAsync
            var httpMessage = await client.GetAsync("http://apress.com");

            //Do other things here while waiting for http request

            //Task continuation
            //Get page length
            //Return a Task object that (when the task is complete) will return the length of ContentLength
            /*return httpTask.ContinueWith((Task<HttpResponseMessage> antecedent) => {
                return antecedent.Result.Content.Headers.ContentLength; });
                */

            return httpMessage.Content.Headers.ContentLength;
        }
    }
}